# PyWASM

An attempt to compile Python to WASM.

- [install emscripten](https://webassembly.org/getting-started/developers-guide/)

```
$ git clone https://github.com/juj/emsdk.git
$ cd emsdk
$ ./emsdk install latest
$ ./emsdk activate latest
$ source ./emsdk_env.sh --build=Release

```

- [install just](https://github.com/casey/just#installation)

- `git submodule update --init`
- `just build`
- `just run`
