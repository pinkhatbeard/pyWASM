clean:
	(cd micropython/ports/minimal && rm -rf build)

build: clean
	(cd micropython/ports/minimal && emmake make)
	(cd micropython/ports/minimal && EMCC_DEBUG=1 emcc -s ASSERTIONS=2 build/firmware.bc -o final.html -s WASM=1)

@run:
	# emrun --no_browser --port 8080 .
	(cd micropython/ports/minimal && node final.js)

opt:
	(cd micropython/ports/minimal && wasm-opt final.wasm -Os -o min.wasm)
	(cd micropython/ports/minimal && mv final.wasm final.wasm.bak)
	(cd micropython/ports/minimal && mv min.wasm final.wasm)
